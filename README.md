# Rodavis API

## API ADDRESS = https://rodavis-api-va624giyma-et.a.run.app

- [User](#User)
  - [register](#register)
  - [login](#login)
  - [get user](#get-user)
- [Report](#Report)
  - [new report](#new-report)
  - [get all report](#get-all-report)
  - [get all user report](#get-all-user-report)
  - [update report](#update-report)

# User
- ## Register
  - **Endpoint**:
    /api/users/register
  - **Method**: POST
  - **Query Param**
    - None
  - **Query String**
    - None
  - **Headers**
    - Content-Type: application/json
  - **Success Status Code**: 201
  - **Error Status Code**: 400, 409, 500
  - **Body**:
    - name (string): minimum characters 4
    - phoneNumber (string): minimum 10 and maximum 15 character. Unique
    - email (string): valid and unique
    - password (string): minimum characters 8
```
{
    "name": "bambank",
    "phoneNumber": "+6281140458912",
    "email": "bambank@gmail.com",
    "password": "123456678"
}
```

  - **Success Response**:
    - status: int
    - message: string
    - data: object
      - user: object
          - id: int
          - name: string
          - phoneNumber: string
          - email: string
          - createdAt: string
          - updatedAt: string
      - token: str
```
{
    "status": 201,
    "message": "Created",
    "data": {
        "user": {
            "id": 1,
            "name": "bambank",
            "phoneNumber": "+6281140458912",
            "email": "bambank@gmail.com",
            "createdAt": "2021-05-20T06:25:44.678655153+07:00",
            "updatedAt": "2021-05-20T06:25:44.678655153+07:00"
        },
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJiYW1iYW5rQGdtYWlsLmNvbSIsImV4cCI6MTYyMjY4MzM2MSwiaWF0IjoxNjIxNDczNzYxLCJzdWIiOiIxIn0.lMegATOIPPJ4sYCoI80IFAHDcsuF1XJZ_mUTP7snFw4"
    }
}
```
  - **Invalid Response**:
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 400,
    "message": "Invalid",
    "errors": [
        "name is too short",
        "password ist too short",
        "email is invalid",
        "phoneNumber is too short"
    ]
}
```

  - **Conflict Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 409,
    "message": "Conflict",
    "errors": [
        "email already used",
        "phone number already used"
    ]
}
```
  - **Server Error Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 500,
    "message": "Internal Server Error",
    "errors": [
        "server error, try again later"
    ]
}
```

- ## Login
  - **Endpoint**:
    /api/users/login
  - **Method**: POST
  - **Query Param**
    - None
  - **Query String**
    - None
  - **Headers**
    - Content-Type: application/json
  - **Success Status Code**: 200
  - **Error Status Code**: 400, 500
  - **Body**:
    - email (string): valid
    - password (string): minimum characters 8
```
{
    "email": "bambank@gmail.com"
    "password": "123456678"
}
```

  - **Success Response**:
    - status: int
    - message: string
    - data: object
      - user: object
          - id: int
          - name: string
          - phoneNumber: string
          - email: string
          - createdAt: string
          - updatedAt: string
      - token: str
```
{
    "status": 201,
    "message": "Created",
    "data": {
        "user": {
            "id": 1,
            "name": "bambank",
            "phoneNumber": "+6281140458912",
            "email": "bambank@gmail.com",
            "createdAt": "2021-05-20T06:25:44.678655153+07:00",
            "updatedAt": "2021-05-20T06:25:44.678655153+07:00"
        },
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJiYW1iYW5rQGdtYWlsLmNvbSIsImV4cCI6MTYyMjY4MzM2MSwiaWF0IjoxNjIxNDczNzYxLCJzdWIiOiIxIn0.lMegATOIPPJ4sYCoI80IFAHDcsuF1XJZ_mUTP7snFw4"
    }
}
```
  - **Invalid Response**:
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 400,
    "message": "Invalid",
    "errors": [
        "email or password is invalid"
    ]
}
```

  - **Server Error Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 500,
    "message": "Internal Server Error",
    "errors": [
        "server error, try again later"
    ]
}
```

- ## Get User
  - **Endpoint**:
    /api/users
  - **Method**: GET
  - **Query Param**
    - None
  - **Query String**
    - None
  - **Headers**
    - Content-Type: application/json
    - Authorization: Bearer `<token>`
  - **Success Status Code**: 200
  - **Error Status Code**: 400, 401, 500
  - **Body**:
    - None
  - **Success Response**:
    - status: int
    - message: string
    - data: object
      - user: object
          - id: int
          - name: string
          - phoneNumber: string
          - email: string
          - createdAt: string
          - updatedAt: string
```
{
    "status": 200,
    "message": "OK",
    "data": {
        "id": 1,
        "name": "bambank",
        "phoneNumber": "+6281140458912",
        "email": "bambank@gmail.com",
        "createdAt": "2021-05-20T06:25:44.678655153+07:00",
        "updatedAt": "2021-05-20T06:25:44.678655153+07:00"    
    }
}
```
  - **Invalid Response**:
    - status: str
    - message: str
    - errors: array of str

- **Not Authorized Response**:
```
{
    "status": 401,
    "message": "Unauthorized",
    "errors": [
        "Not Authorized"
    ]
}
```

- **Not Found Error**
```
{
    "status:": 400,
    "message": "Invalid",
    "errors": [
        "User Not Found"
    ]
}
```

  - **Server Error Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 500,
    "message": "Internal Server Error",
    "errors": [
        "server error, try again later"
    ]
}
```

# Report
- ## New Report
  - **Endpoint**:
    /api/reports
  - **Method**: POST
  - **Query Param**
    - None
  - **Query String**
    - None
  - **Headers**
    - Content-type: multipart/form-data
    - Authorization: Bearer `<token>`
  - **Success Status Code**: 201
  - **Error Status Code**: 400, 401, 500, 503
  - **Body**:
    - lat: (float) required, not empty.
    - lng: (float) required, not empty.
    - image: (jpeg, jpg, png) required
    - note: (str) optional
  - **Success Response**:
    - status: int
    - message: string
    - data: object
      - id: int
      - reporterName: str
      - dateReported: str
      - status: str
      - imageUrl: str (url)
      - classes: array of str (enum of ["D00", "D01", "D10", "D11", "D20", "D40", "D43", "D44", "D50"])
      - note: str
      - location: object
        - lat: float
        - lng: float
```
{
    "status": 201,
    "message": "Created",
    "data": {
        "id": 1,
        "reporterName": "bambank",
        "dateReported": "2021-05-20T06:25:44.678655153+07:00"
        "status": "Reported",
        "imageUrl": "https://storage.googleapis.com/rodavis-image/2021-05-30_10:15:00_pontholes_test4.jpg",
        "classes": ["D40"],
        "note": "tolong segera diperbaiki karena anak saya geblak di situ",
        "location": {
            "lat": -7.666369905243495
            "lng": 110.66331442645793
        }
    }
}
```
  - **Invalid Response**:
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 400,
    "message": "Invalid",
    "errors": [
        "invalid image format"
    ]
}
```
- **Not Authorized Response**:
```
{
    "status": 401,
    "message": "Unauthorized",
    "errors": [
        "Not Authorized"
    ]
}
```
  - **Service Unavailable Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 503,
    "message": "Service Unavailable",
    "errors": [
        "Prediction Service Unavailable. Service might be still starting up. Try Again later."
    ]
}

```
  - **Server Error Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 500,
    "message": "Internal Server Error",
    "errors": [
        "server error, try again later"
    ]
}
```
- ## Get All Report
  - **Description**: <br>Return list of reports from newest to oldest. By default will return all reports if query string is not present.
  - **Endpoint**:
    /api/reports
  - **Method**: GET
  - **Query Param**
    - None
  - **Query String**
    - limit: int. optional
    - lastseenid: int. optional
  - **Headers**
    - None
  - **Success Status Code**: 200
  - **Error Status Code**: 500
  - **Body**:
    - None
  - **Success Response**:
    - status: int
    - message: string
    - data: array of object
      - id: int
      - reporterName: str
      - dateReported: str
      - status: str (enum one of ["Reported", "Under Repair",  "Completed", "Rejected"])
      - imageUrl: str (url),
      - classes: array of str (enum of ["D00", "D01", "D10", "D11", "D20", "D40", "D43", "D44", "D50"])
      - note: str
      - address: str
      - location: object
        - lat: float
        - lng: float
```
{
    "status": 200,
    "message": "OK",
    "data": [
        {
            "id": 2,
            "reporterName": "paijo",
            "dateReported": "2021-05-20T06:25:44.678655153+07:00"
            "status": "Under Repair",
            "imageUrl": "https://storage.googleapis.com/rodavis-image/2021-05-30_10:15:00_pontholes_test4.jpg",
            "classes": ["D40"],
            "note": "tolong segera diperbaiki karena anak saya ndlungup di situ",
            "address": "mataram",
            "location": {
                "lat": 37.24868777461834
                "lng": -115.80754896920976
            }
        },
        {
            "id": 1,
            "reporterName": "bambank",
            "dateReported": "2021-05-20T06:25:44.678655153+07:00"
            "status": "Reported",
            "imageUrl": "https://storage.googleapis.com/rodavis-image/2021-05-30_10:42:28_dalan.jpg",
            "classes": ["D40"],
            "note": "tolong segera diperbaiki karena anak saya geblak di situ",
            "address": "mataram",
            "location": {
                "lat": -7.666369905243495
                "lng": 110.66331442645793
            }
        }
    ]
}
```
  - **Empty Report Response**:
    - status: str
    - message: str
    - data: array of object
```
{
    "status:": 200,
    "message": "OK",
    "data": []
}
```
  - **Server Error Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 500,
    "message": "Internal Server Error",
    "errors": [
        "server error, try again later"
    ]
}
```
- ## Get All User Report
  - **Description**: <br>
    Return list of reports that the user have been submitted from newest to oldest. By default will return all reports if query string is not present.
  - **Endpoint**:
    /api/reports/history
  - **Method**: GET
  - **Query Param**
    - None
  - **Query String**
    - limit: int. optional
    - lastseenid: int. optional
  - **Headers**
    - Authorization: Bearer `<token>`
  - **Success Status Code**: 200
  - **Error Status Code**: 500
  - **Body**:
    - None
  - **Success Response**:
    - status: int
    - message: string
    - data: array of object
      - id: int
      - reporterName: str
      - dateReported: str
      - status: str (enum one of ["Reported", "Under Repair",  "Completed", "Rejected"])
      - imageUrl: str (url),
      - classes: array of str (enum of ["D00", "D01", "D10", "D11", "D20", "D40", "D43", "D44", "D50"])
      - note: str
      - address: str
      - location: object
        - lat: float
        - lng: float
```
{
    "status": 200,
    "message": "OK",
    "data": [
        {
            "id": 4,
            "reporterName": "bambank",
            "dateReported": "2021-05-20T06:25:44.678655153+07:00"
            "status": "Under Repair",
            "imageUrl": "https://storage.googleapis.com/rodavis-image/2021-05-30_10:15:00_pontholes_test4.jpg",
            "classes": ["D40"],
            "note": "",
            "address": "mataram",
            "location": {
                "lat": -6.983943
                "lng": 110.410378
            }
        },
        {
            "id": 2,
            "reporterName": "paijo",
            "dateReported": "2021-05-20T06:25:44.678655153+07:00"
            "status": "Under Repair",
            "imageUrl": "https://storage.googleapis.com/rodavis-image/2021-05-30_10:15:00_pontholes_test4.jpg",
            "classes": ["D40"],
            "note": "tolong segera diperbaiki karena anak saya ndlungup di situ",
            "address": "mataram",
            "location": {
                "lat": 37.24868777461834
                "lng": -115.80754896920976
            }
        },
        {
            "id": 1,
            "reporterName": "bambank",
            "dateReported": "2021-05-20T06:25:44.678655153+07:00"
            "status": "Reported",
            "imageUrl": "https://storage.googleapis.com/rodavis-image/2021-05-30_10:15:00_pontholes_test4.jpg",
            "classes": ["D40"],
            "note": "tolong segera diperbaiki karena anak saya geblak di situ",
            "address": "mataram",
            "location": {
                "lat": -7.666369905243495
                "lng": 110.66331442645793
            }
        }
    ]
}
```
  - **Empty Report Response**:
    - status: str
    - message: str
    - data: array of object
```
{
    "status:": 200,
    "message": "OK",
    "data": []
}
```
  - **Server Error Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 500,
    "message": "Internal Server Error",
    "errors": [
        "server error, try again later"
    ]
}
```
- ## Update Report
  - **Description**: <br>
    Update status of report. Must have role of admin to update.
  - **Endpoint**:
    /api/reports/{reportId}
  - **Method**: GET
  - **Query Param**
    - reportId: int. required
  - **Query String**
    - limit: int. optional
    - lastseenid: int. optional
  - **Headers**:
    - Content-Type: application/json
    - Authorization: Bearer `<token>`
  - **Success Status Code**: 200
  - **Error Status Code**: 400, 401, 404, 500
  - **Body**:
    - status: str. required (enum one of ["Under Repair",  "Completed", "Rejected"])
  - **Success Response**:
    - status: int
    - message: string
    - data: object
      - id: int
      - reporterName: str
      - dateReported: str
      - status: str (enum one of ["Reported", "Under Repair",  "Completed", "Rejected"])
      - imageUrl: str (url),
      - classes: array of str (enum of ["D00", "D01", "D10", "D11", "D20", "D40", "D43", "D44", "D50"])
      - note: str
      - address: str
      - location: object
        - lat: float
        - lng: float
```
{
    "status": 200,
    "message": "OK",
    "data": {
              "id": 1,
              "reporterName": "bambank",
              "dateReported": "2021-05-20T06:25:44.678655153+07:00"
              "status": "Completed",
              "imageUrl": "https://storage.googleapis.com/rodavis-image/2021-05-30_10:15:00_pontholes_test4.jpg",
              "classes": ["D40"],
              "note": "tolong segera diperbaiki karena anak saya geblak di situ",
              "address": "mataram",
              "location": {
                  "lat": -7.666369905243495
                  "lng": 110.66331442645793
            }
        }
}
```
  - **Invalid Response**:
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 400,
    "message": "Invalid",
    "errors": [
        "status is required"
    ]
}
```
  - **Not Found Response**:
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 404,
    "message": "Not Found",
    "errors": [
        "report not found"
    ]
}
```
- **Not Authorized Response**:
```
{
    "status": 401,
    "message": "Unauthorized",
    "errors": [
        "Not Authorized"
    ]
}
```

  - **Server Error Response**
    - status: str
    - message: str
    - errors: array of str
```
{
    "status:": 500,
    "message": "Internal Server Error",
    "errors": [
        "server error, try again later"
    ]
}
```